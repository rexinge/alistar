import flarum from '../../flarum';
import {urlEncode} from "../../utils/tools";

/**
 * 每页显示
 * @type {number}
 */
const LIMIT = 20;

/**
 * 关于论坛的信息
 * @returns {"https://discuss.flarum.org.cn/api"}
 */
const getApi = () => {
    return `${flarum.api}`
}

/**
 * 获取所有的讨论
 * @param sort
 * @param offset
 * @param include
 * @returns {string}
 */
const getDiscussionsList = (sort = "",
                            offset = 0,
                            include = "user,lastPostedUser,tags,tags.parent,firstPost") => {
    return urlEncode(`${flarum.api}/discussions?include=${include}&sort=${sort}&page[offset]=${offset}&page[limit]=${LIMIT}`)
}

/**
 * 通过 ID 获取讨论
 * @param id
 * @param near
 * @returns {string}
 */
const getDiscussions = (id, near = 0) => {
    return urlEncode(`${flarum.api}/discussions/${id}?page[near]=${near}`)
}

/**
 * 创建讨论
 * @returns {"https://discuss.flarum.org.cn/api/discussions"}
 */
const createDiscussions = () => {
    return `${flarum.api}/discussions`
}

/**
 * 删除一个讨论
 * @param id
 * @returns {`https://discuss.flarum.org.cn/api/discussions/${string}`}
 */
const deleteDiscussions = (id) => {
    return `${flarum.api}/discussions/${id}`
}

/**
 * 更新一个讨论
 * @param id
 * @returns {`https://discuss.flarum.org.cn/api/discussions/${string}`}
 */
const modifiedDiscussions = (id) => {
    return `${flarum.api}/discussions/${id}`
}

/**
 * 获取评论列表
 * @param author
 * @param type
 * @param sort
 * @param offset
 * @returns {string}
 */
const getPosts = (author = "",
                  type = "comment",
                  sort = "createdAt",
                  offset = 0) => {
    return urlEncode(`${flarum.api}/posts?filter[author]=${author}&filter[type]=${type}&page[offset]=${offset}&page[limit]=${LIMIT}&sort=-${sort}`)
}

/**
 * 修改评论
 * @param id
 * @returns {`https://discuss.flarum.org.cn/api/posts/${string}`}
 */
const modifiedPosts = (id) => {
    return `${flarum.api}/posts/${id}`
}

/**
 * 创建一个新的评论
 * @returns {"https://discuss.flarum.org.cn/api/posts"}
 */
const createPosts = () => {
    return `${flarum.api}/posts`
}

/**
 * 获取我的文件
 * @param uid
 * @param offset
 * @returns {string}
 */
const getUploads = (uid, offset = 0) => {
    return urlEncode(`${flarum.api}/fof/uploads?filter[user]=${uid}&page[offset]=${offset}`)
}

/**
 * 获取用户资料
 * @param uid
 * @returns {`https://discuss.flarum.org.cn/api/users/${string}`}
 */
const getProfiles = (uid) => {
    return `${flarum.api}/users/${uid}`
}

/**
 * 修改用户资料
 * @param uid
 * @returns {`https://discuss.flarum.org.cn/api/users/${string}`}
 */
const modifiedProfiles = (uid) => {
    return `${flarum.api}/users/${uid}`
}

/**
 * 获取通知列表
 * @param offset
 * @returns {string}
 */
const getNotifications = (offset = 0) => {
    return urlEncode(`${flarum.api}/notifications?page[offset]=${offset}`)
}

/**
 * 将通知标记为已读，全部通知
 * @returns {"https://discuss.flarum.org.cn/api/notifications/read"}
 */
const readNotifications = () => {
    return `${flarum.api}/notifications/read`
}

/**
 * 获取TAGS列表
 * @param include
 * @returns {string}
 */
const getTags = (include = "children,lastPostedDiscussion,parent") => {
    return urlEncode(`${flarum.api}/tags?include=${include}`)
}

/**
 * 获取请求Header头
 * @returns {{}}
 */
const getRequestHeader = (headers = {}) => {
    let header = {}
    header["content-type"] = "application/json"
    header["Authorization"] = `Token`
    return Object.assign(header, headers)
}

/**
 * 注册新用户
 * @returns {"https://discuss.flarum.org.cn/api/api/users"}
 */
const register = () => {
    return `${flarum.api}/api/users`
}

/**
 * 身份验证
 * @returns {"https://discuss.flarum.org.cn/api/token"}
 */
const token = () => {
    return `${flarum.api}/token`
}

/**
 * 搜索用户
 * @param q
 * @returns {string}
 */
const searchUsers = (q) => {
    return getUsers(q)
}

/**
 * 获取用户列表
 * @param q
 * @param offset
 * @returns {string}
 */
const getUsers = (q = "",
                  offset = 0) => {
    return urlEncode(`${flarum.api}/api/users?filter[q]=${q}&page[limit]=${LIMIT}&page[offset]=${offset}`)
}

/**
 * 删除头像
 * @param uid
 * @returns {`https://discuss.flarum.org.cn/api/api/users/${string}/avatar`}
 */
const deleteAvatar = (uid) => {
    return `${flarum.api}/api/users/${uid}/avatar`
}

/**
 * 删除头像
 * @param uid
 * @returns {`https://discuss.flarum.org.cn/api/api/users/${string}/avatar`}
 */
const modifiedAvatar = (uid) => {
    return `${flarum.api}/api/users/${uid}/avatar`
}

/**
 * @type {{getRequestHeader: (function(*=): unknown), modifiedAvatar: (function(*): string), getPosts: (function(*=, *=, *=, *=): string), modifiedDiscussions: (function(*): string), getNotifications: (function(*=): string), createPosts: (function(): string), getApi: (function(): string), createDiscussions: (function(): string), searchUsers: (function(*=): string), getDiscussionsList: (function(*=, *=, *=): string), getDiscussions: (function(*, *=): string), token: (function(): string), modifiedPosts: (function(*): string), deleteAvatar: (function(*): string), modifiedProfiles: (function(*): string), getUploads: (function(*, *=): string), getUsers: (function(*=, *=): string), deleteDiscussions: (function(*): string), getProfiles: (function(*): string), readNotifications: (function(): string), register: (function(): string)}}
 */
module.exports = {
    getApi,
    getPosts,
    getDiscussionsList,
    getDiscussions,
    deleteDiscussions,
    createDiscussions,
    modifiedDiscussions,
    modifiedPosts,
    createPosts,
    getUploads,
    getUsers,
    getProfiles,
    modifiedProfiles,
    getNotifications,
    readNotifications,
    getRequestHeader,
    register,
    token,
    searchUsers,
    deleteAvatar,
    modifiedAvatar
}