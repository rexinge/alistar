import JSONAPISerializer from "json-api-serializer"
import request from "../../utils/request";
import flarum from "../../flarum";

export class Users {
    constructor() {
        this.serializer = new JSONAPISerializer()
    }

    registerProfiles() {
        this.serializer.register("users", {
            id: "id",
            relationships: {
                userBadges: {
                    type: "userBadges"
                },
                badges: {
                    type: "badges"
                },
                badgeCategories: {
                    type: "badgeCategories"
                },
                groups: {
                    type: "groups"
                }
            }
        })

        this.serializer.register('userBadges', {
            type: "userBadges",
            id: "id"
        })

        this.serializer.register('badges', {
            type: "badges",
            id: "id"
        })

        this.serializer.register('badgeCategories', {
            type: "badgeCategories",
            id: "id"
        })

        this.serializer.register('groups', {
            type: "groups",
            id: "id"
        })
    }

    getProfiles(id, func) {
        const that = this
        const url = `${flarum.api}/users/${id}`
        request.get(url).then(res => {
            that.registerProfiles()
            that.data   = res
            that.viewdata = that.serializer.deserialize("users", res)
            func(that.data, that.viewdata)
        }).catch(res => {
            console.log(res)
        })
    }
}