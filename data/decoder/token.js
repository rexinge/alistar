import JSONAPISerializer from "json-api-serializer"
import request from "../../utils/request";
import flarum from "../../flarum";
import {urlEncode} from "../../utils/tools";

export class Token {
    constructor() {
    }

    /**
     * 获取API地址
     * @returns {string}
     */
    getApi() {
        return `${flarum.api}/token`
    }

    /**
     * 执行
     * @param username
     * @param password
     * @param func
     */
    execute(username, password, func) {
        const that = this
        const url = that.getApi()
        request.post(url, {
            "identification": username,
            "password": password
        }, {
            "content-type": "application/vnd.api+json"
        }).then(res => {
            func(res, res)
        }).catch(res => {
            console.log("res", res)
        })
    }
}