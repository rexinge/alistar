/**
 * 讨论数据封装
 * @param id
 * @param subscription
 * @returns {string}
 */
const discussions = (id, subscription = "follow") => {
    let json = {
        data: {
            type: "discussions",
            id: id,
            attributes: {
                subscription: subscription
            }
        }
    }
    return JSON.stringify(json)
}

/**
 * 评论数据封装
 * @param id
 * @param attributes
 */
const posts = (id, attributes = {}) => {
    let json = {
        data: {
            type: "posts",
            id: id,
            attributes: attributes
        }
    }
    return JSON.stringify(json)
}

/**
 * 关注一个讨论
 * @param id
 * @returns {string}
 */
const followDiscussions = (id) => {
    return discussions(id, "follow")
}

/**
 * 取消关注/取消忽略一个讨论
 * @param id
 * @returns {string}
 */
const unNullDiscussions = (id) => {
    return discussions(id, null)
}

/**
 * 忽略一个讨论
 * @param id
 * @returns {string}
 */
const ignoreDiscussions = (id) => {
    return discussions(id, "ignore")
}

/**
 * 点赞一个评论
 * @param id
 * @returns {string}
 */
const likePosts = (id) => {
    return posts(id, {isLiked: true})
}

/**
 * 取消点赞一个评论
 * @param id
 * @returns {string}
 */
const unlikePosts = (id) => {
    return posts(id, {isLiked: false})
}

module.exports = {
    followDiscussions,
    unNullDiscussions,
    ignoreDiscussions,
    likePosts,
    unlikePosts
}