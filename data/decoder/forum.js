import JSONAPISerializer from "json-api-serializer"
import request from "../../utils/request";
import flarum from "../../flarum";

export class Forum {

    constructor() {
        this.serializer = new JSONAPISerializer()
        this.serializer.register("forums", {
            id: "id",
            relationships: {
                reactions: {
                    type: "reactions"
                },
                forums: {
                    type: "forums"
                },
                groups: {
                    type: "groups"
                },
                links: {
                    type: "links"
                },
                tags: {
                    type: "tags"
                },
                "fof-terms-policies": {
                    type: "fof-terms-policies"
                }
            }
        })
        this.serializer.register("forums", {
            id: "id"
        })

        this.serializer.register("groups", {
            id: "id"
        })
        this.serializer.register("links", {
            id: "id"
        })
        this.serializer.register("tags", {
            id: "id"
        })
        this.serializer.register("fof-terms-policies", {
            id: "id"
        })
        this.serializer.register("reactions", {
            id: "id"
        })
    }

    /**
     * 获取API地址
     * @returns {string}
     */
    getApi() {
        return `${flarum.api}`
    }

    /**
     * 执行请求
     * @param func
     */
    execute(func) {
        const that = this
        request.get(that.getApi()).then(res => {
            that.data = res
            that.viewdata = that.serializer.deserialize("forums", res)

            func(that.data, that.viewdata)
        }).catch(res => {})
    }
}