const {urlEncode} = require("./tools");

/**
 * 跳转到标签页
 * @param e
 */
const gotoTagPage = (e) => {
    const description = e.currentTarget.dataset.description,
        icon = e.currentTarget.dataset.icon,
        slug = e.currentTarget.dataset.slug,
        name = e.currentTarget.dataset.name,
        id = e.currentTarget.dataset.id
    const url = `/pages/threads/tags/tags?id=${id}&name=${name}&icon=${icon}&slug=${slug}&description=${description}`
    console.log("gotoDiscussionByTags", url)
    wx.navigateTo({
        url: url
    })
}

/**
 * 跳转到资料页
 * @param e
 */
const gotoProfilePage = (e) => {
    const slug = e.currentTarget.dataset.slug,
        username = e.currentTarget.dataset.username,
        nickname = e.currentTarget.dataset.nickname,
        avatar = e.currentTarget.dataset.avatar

    const url = `/pages/users/profiles/profiles?slug=${slug}&username=${username}&nickname=${nickname}&avatar=${urlEncode(avatar)}`
    wx.navigateTo({
        url: url
    })
}

/***
 * 解析A标记
 * @param link
 */
const analysisLink = (link) => {
    console.log('handleTagATap', link)
    let re = new RegExp()
}

/**
 * 跳转到创建话题页面
 */
const gotoNewDiscussionPage = () => {
    const url = "/pages/threads/discussion/discussion"
    wx.navigateTo({
        url: url
    })
}

/**
 * 跳转到讨论详细页面
 * @param e
 */
const gotoDetailPage = (e) => {
    const id = e.currentTarget.dataset.id,
        title = e.currentTarget.dataset.title,
        tags = e.currentTarget.dataset.tags,
        slug = e.currentTarget.dataset.slug,
        firstpost = e.currentTarget.dataset.firstpost,
        users = e.currentTarget.dataset.users,
        sticky = e.currentTarget.dataset.sticky

    let url = `/pages/threads/detail/detail?sticky=${sticky}&firstpost=${firstpost}&users=${users}&id=${id}&title=${title}&tags=${tags}&slug=${slug}`
    wx.navigateTo({
        url: url
    })
}

const gotoIndex = () => {
    const url = "/pages/navigator/index/index"
    wx.switchTab({
        url: url
    })
}

const gotoLogin = () => {
    const url = "/pages/users/login/login"
    wx.navigateTo({
        url: url
    })
}

const gotoSetting = () => {
  
}

module.exports = {
    gotoTagPage,
    gotoProfilePage,
    analysisLink,
    gotoNewDiscussionPage,
    gotoDetailPage,
    gotoIndex,
    gotoLogin,
    gotoSetting
}