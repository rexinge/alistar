# Alistar

![](screenshot/logo.png)

[![star](https://gitee.com/rexinge/alistar/badge/star.svg?theme=dark)](https://gitee.com/rexinge/alistar/stargazers)
[![fork](https://gitee.com/rexinge/alistar/badge/fork.svg?theme=dark)](https://gitee.com/rexinge/alistar/members)

#### 介绍
flarum 小程序

##### 目录结构
```
|-- Alistar
    |-- .gitignore
    |-- LICENSE
    |-- README.md
    |-- app.js
    |-- app.json
    |-- app.wxss
    |-- directoryList.md
    |-- fix.js
    |-- flarum.js
    |-- package-lock.json
    |-- package.json
    |-- project.config.json
    |-- sitemap.json
    |-- .github
    |   |-- ISSUE_TEMPLATE
    |       |-- submit-form.yaml
    |-- assets
    |   |-- fonts
    |   |-- images
    |   |   |-- navigator
    |   |-- wxss
    |       |-- flarum.config.wxss
    |       |-- flarum.wxss
    |       |-- font-awesome.wxss
    |       |-- spacing.wxss
    |-- components
    |   |-- fa
    |-- custom-tab-bar
    |-- data
    |   |-- app
    |   |   |-- app.js
    |   |-- decoder
    |       |-- discussions.js
    |       |-- forum.js
    |       |-- notifications.js
    |       |-- posts.js
    |       |-- register.js
    |       |-- search.js
    |       |-- tags.js
    |       |-- token.js
    |       |-- uploads.js
    |       |-- users.js
    |-- miniprogram_npm
    |-- pages
    |   |-- tabbar.js
    |   |-- navigator
    |   |   |-- index
    |   |   |-- notification
    |   |   |-- profiles
    |   |   |-- tags
    |   |-- search
    |   |-- threads
    |   |   |-- detail
    |   |   |-- discussion
    |   |-- users
    |       |-- login
    |       |-- password
    |       |   |-- find
    |       |-- register
    |       |-- settings
    |-- screenshot
    |-- test
    |   |-- flarum.js
    |   |-- json.js
    |-- utils
        |-- api.js
        |-- data.js
        |-- request.js
        |-- tools.js
        |-- util.js
```

#### 截图


#### 安装教程

1. 修改配置文件，将根目录下`flarum.js` 中`api` 修改为你的域名api；
2. 运行`npm install` 安装必要的依赖
3. 打开小程序IDE工具->npm构建，让小程序构建npm依赖；
4. 修改`project.config.json`中`appid`值为自己小程序appid；、
5. `assets/wxss/flarum.config.wxss` 为相关颜色配置，若需要直接修改为自己想要的结果；

#### 参与贡献

1.  Fork 本仓库
2.  新建 feature_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 感谢
- 富文本编辑器 https://gitee.com/kogami123/applet_rich_text_editor
- HTML 展示器 