import tabbar from '../../tabbar';
import {gotoLogin, gotoSetting} from "../../../utils/navigate";
import {Users} from "../../../data/decoder/users";
import {Uploads} from "../../../data/decoder/uploads";
import {Discussions} from "../../../data/decoder/discussions";
import {Posts} from "../../../data/decoder/posts";
import {checkLoginStatus} from "../../../utils/util";

const app = getApp()
Page({
    data: {
        threads: [],
        list: tabbar,
        statusBarHeight: app.globalData.statusBarHeight,
        isLogin: false,
        uid: 0,
        page: 1,
        profile: {},
        type: "threads",
        uploads: [],
        height: 0,
    },
    onShow: function () {
        const that = this
        const loginStatus = checkLoginStatus()
        console.log('loginStatus', loginStatus)
        that.getH()
        that.setData({
            isLogin: loginStatus.isLogin,
            uid: loginStatus.uid,
        })
        // 如果 没有登录，我就退出
        // TODO
        if (loginStatus.isLogin === false) {
            that.setData({
                profile: {},
            })
        } else {
            that.loadProfile()
        }
    },
    onLoad: function (options) {
        const that = this
        const loginStatus = checkLoginStatus()
        that.setData({
            isLogin: loginStatus.isLogin,
            uid: loginStatus.uid,
        })
        if (loginStatus.isLogin) {
            that.loadProfile()
        }
        that.getH()
    },
    loadProfile: function() {
        const that = this
        const profile = new Users()
        profile.getProfiles(that.data.uid, function(data, viewData) {
            that.setData({
                profile: viewData,
            })
            that.loadData()
        })
    },
    changeTabs: function(e) {
        const currentType = e.detail.activeKey
        const that = this
        console.log('currentType',currentType , 'data.type', that.data.type)
        if (currentType !== that.data.type) {
            that.setData({
                type: currentType,
                page: 1,
            })
            that.loadData()
        }
    },
    loadData: function(isAppend = false) {
        const that = this
        if (that.data.isLogin === false) {
            console.log("Your need login first")
            return
        }
        const type = that.data.type
        const username = that.data.profile.username
        if (type === "threads") {
            const offset = (that.data.page - 1) * 20
            const discussions = new Discussions()
            discussions.execute({offset: offset, author: username}, function(data, viewData) {
                if (isAppend === false){
                    that.setData({
                        threads: viewData
                    })
                } else {
                    that.setData({
                        threads: that.data.threads.concat(viewData)
                    })
                }
            })
        } else if (type === "replies") {
            const offset = (that.data.page - 1) * 20
            const posts = new Posts()
            posts.execute({
                author: username,
                type: "comment",
                sort: "-createdAt",
                offset: offset
            }, function(data, viewData) {
                console.log(viewData)
                if (isAppend === false){
                    that.setData({
                        comments: viewData
                    })
                } else {
                    that.setData({
                        comments: that.data.comments.concat(viewData)
                    })
                }
            })
        } else if (type === "files") {
            const uploads = new Uploads()
            uploads.getUploads(that.data.uid, 0, function(data, viewData, options){
                that.setData({
                    uploads: that.data.uploads.concat(viewData)
                })
            })
        }
    },
    gotoDetail: function (e) {
    },
    flarumGotoLoginTap: function(e) {
        console.log(e)
        gotoLogin()
    },
    gotoSetting: function() {
        gotoSetting()
    },
    getH: function() {
        //高度自适应
        const that = this;
        wx.getSystemInfo({
            success: function (res) {
                const clientHeight = res.windowHeight,
                    clientWidth = res.windowWidth,
                    rpxR = 750 / clientWidth;
                const h = clientHeight *0.5* rpxR;
                that.setData({
                    height: h
                });
            }
        });
    }
});