import {urlDecode} from "../../../utils/tools";
import {Users} from "../../../data/decoder/users";

Page({
    data: {
        statusBarHeight: getApp().globalData.statusBarHeight,
        profile: {}
    },
    onLoad: function (options) {
        const slug = options.slug,
            username = options.username,
            nickname = options.nickname,
            avatar = options.avatar ? urlDecode(options.avatar) : "",
            that = this
        that.setData({
            profile: {
                "username": username,
                "displayName": nickname,
                "avatarUrl": avatar,
                "slug": slug,
            },
        })
        that.loadData()
    },
    onPullDownRefresh() {
        const that = this
        that.loadData()
    },
    loadData: function() {
        const that = this
        const users = new Users()
        users.getProfiles(that.data.profile.slug, function(data, viewData) {
            console.log(viewData)
            that.setData({
                profile: viewData
            })
            wx.stopPullDownRefresh()
        })
    },
});