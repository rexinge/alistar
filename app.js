import {Forum} from "data/decoder/forum"
import flarum from "./flarum";
import {getStorage, setStorage} from "./utils/util";

// app.js
App({
  onLaunch() {
    const that = this
    that.getFlarum()

    wx.getSystemInfo({
      success: function (res) {
        that.globalData.statusBarHeight = res.statusBarHeight
        that.globalData.language = res.language
        that.globalData.version = res.version
        that.globalData.platform = res.platform
      }
    })

    const identity = getStorage(flarum.cache.identity)
    if (identity) {
      that.globalData.userInfo.identity = identity
    }
  },
  getFlarum: function () {
    const that = this
    try {
      let flarumInfo = getStorage(flarum.cache.flarum)
      if (flarumInfo && flarumInfo !== "") {
        that.globalData.flarum = flarum
      } else {
        const forum = new Forum()
        forum.execute(function(data, viewData) {
          // 进行存储缓存了
          setStorage(flarum.cache.flarum, viewData)
          that.globalData.flarum = viewData
        })
      }
    } catch (e) {
    }
  },
  globalData: {
    userInfo: {},
    flarum: {},
    statusBarHeight: 44
  }
})
