Component({
    properties: {
        icon: null,
        type: String,
        color: {
            type:String
        },
        size: {
            type: null,
            value: 28
        }
    },
    data: {},
    methods: {}
});