const urlEncode = (url) => {
    return encodeURIComponent(url)
}

const urlDecode = (string) => {
    return decodeURIComponent(string)
}

/**
 * 删除html标签
 * @param html
 * @returns {*}
 */
const deleteHtmlTags = (html) => {
    return html.replace(/<[^>]+>/g,"")
}

/**
 * @type {{urlDecode: (function(*=): string), gotoTarget: gotoTarget, urlEncode: (function(*=): string)}}
 */
module.exports = {
    urlEncode,
    urlDecode,
    deleteHtmlTags
}