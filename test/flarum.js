const json = require("./json");
const JSONAPISerializer = require("json-api-serializer");
const Serializer = new JSONAPISerializer();


Serializer.register("discussions", {
    id: "id",
    relationships: {
        users: {
            type: "users"
        },
        tags: {
            type: "tags"
        },
        posts: {
            type: "posts"
        }
    },
    topLevelMeta: function(data, extraData) {
        return {
            count: extraData.count,
            total: data.length
        };
    },
    topLevelLinks: function(data, extraData) {
        return {
            "first": extraData.first
        };
    }
})

Serializer.register('users', {
    type: "users",
    id: "id"
})

Serializer.register('tags', {
    type: "tags",
    id: "id"
})

Serializer.register('posts', {
    type: "posts",
    id: "id"
})

Serializer.deserializeAsync('discussions', json.json).then(res => {
    console.log(res)
}).catch(res => {
    console.error("Error", res)
});