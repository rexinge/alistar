import flarum from "../../../flarum";
import {gotoLogin} from "../../../utils/navigate";
import {checkLoginStatus} from "../../../utils/util";

Component({
    data: {
        name: "",
        isLogin: false,
        status: false
    },
    properties: {
        profile: {
            type: Object,
            value: {}
        },
        status: {
            type: Boolean,
            value: false
        }
    },
    attached: function() {
        const that = this
        let name = ""
        switch (flarum.driver.displayName){
            case "nickname":
                name = that.properties.profile.displayName
                break
            case "username":
            default:
                name = that.properties.profile.username
                break
        }

        // 如果需要检测登录状态
        let status = that.properties.status
        let loginStatus = checkLoginStatus()

        that.setData({
            name: name,
            isLogin: loginStatus.isLogin,
            status: status
        })
    },
    externalClasses: ['flarum-username-class'],
    methods: {
        flarumNameTap: function(e) {
            // 采用递归组件方式渲染，不能通过triggerEvent方式向父级传参，可以获取当前页面调用页面方法处理
            const curPages = getCurrentPages();
            const currentPage = curPages[curPages.length - 1];
            if (currentPage && currentPage.flarumNameTap) {
                currentPage.flarumNameTap(e);
            }
        },
        flarumGotoLoginTap: function(e) {
            gotoLogin()
        }
    }
});
