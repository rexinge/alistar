export default {
    title: "豆瓣君",
    baseUrl: "https://qizue.com",
    api: "https://qizue.com/api",
    showLoadingReachBottom: false, // 上拉加载更多是否展示loading弹窗
    copyOtherLink: true, //如果是站外链接 是否直接复制
    cache: {
        identity: "users-identity",
        flarum: "flarum"
    },
    driver: {
        displayLogin: "favicon", // 支持 logo和 favicon
        displayName: "username", //昵称显示
        defaultAvatar: "/assets/images/avatar-default.png", //默认头像地址，可以是网络地址
    },
    request: {
        timeout: 2000,
        enableCache: false,
        responseType: "text",
        dataType: "json"
    }
}