import JSONAPISerializer from "json-api-serializer"
import request from "../../utils/request";
import flarum from "../../flarum";
import {formatOptions} from "../../utils/util";

export class Uploads {
    constructor() {
        this.serializer = new JSONAPISerializer()
    }

    registerFiles() {
        this.serializer.register("files", {
            id: "id"
        })
    }

    /**
     * 获取上传的文件列表
     * @param id
     * @param offset
     * @param func
     */
    getUploads(id, offset = 0, func) {
        const that = this
        const url = `${flarum.api}/fof/uploads?filter[user]=${id}&page[offset]=${offset}`
        request.get(url).then(res => {
            that.registerFiles()
            that.data   = res
            that.viewdata = that.serializer.deserialize("files", res)
            that.options = formatOptions(that.data)
            func(that.data, that.viewdata, that.options)
        }).catch(res => {
            console.log(res)
        })
    }
}