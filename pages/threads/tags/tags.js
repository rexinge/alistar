import {Discussions} from "../../../data/decoder/discussions";
import {deleteHtmlTags, urlEncode} from "../../../utils/tools";
import {gotoDetailPage, gotoProfilePage} from "../../../utils/navigate";

Page({
    data: {
        hasMore: false,
        id: "",
        slug: "",
        page: 1,
        discussions: [],
        loading: false
    },
    onLoad: function (options) {
        const that = this
        console.log('options', options)
        const name = options.name,
            id = options.id,
            slug = options.slug

        wx.setNavigationBarTitle({
            title: name
        })

        that.setData({
            id: id,
            slug: slug,
            page: 1,
            discussions: [],
        })

        that.loadData()
    },
    onReachBottom: function() {
        const that = this
        if (that.data.hasMore === true) {
            console.log("no more data")
            return
        }
        wx.showNavigationBarLoading()
        console.log('onReachBottom start')
        that.setData({
            page: that.data.page + 1
        })
        that.loadData(true)
    },
    onPullDownRefresh: function() {
        const that = this
        that.setData({
            page: 1,
            hasMore: false
        })
        wx.showNavigationBarLoading()
        console.log('onPullDownRefresh start')
        that.loadData()
    },
    loadData: function(isAppend = false) {
        const discussions = new Discussions()
        const that = this
        const offset = (that.data.page - 1) * 20
        discussions.execute({offset: offset, tag: that.data.slug}, function(data, viewData, options) {
            // 如果没有数据返回则，我就认为没有下一页
            if (viewData.length <= 0) {
                that.setData({
                    hasMore: true,
                })
                wx.hideNavigationBarLoading()
                wx.stopPullDownRefresh()
                return
            }
            viewData.forEach(function(item, key){
                viewData[key]["users_json"] = urlEncode(JSON.stringify(item["user"]))
                viewData[key]["tags_json"] = urlEncode(JSON.stringify(item["tags"]))
                viewData[key]["firstpost_json"] = urlEncode(JSON.stringify(item["firstPost"]))

                viewData[key]["firstPost"]["content"] = deleteHtmlTags(item["firstPost"]["contentHtml"])
                viewData[key]["firstPost"]["content"] = `${viewData[key]["firstPost"]["content"].substring(0, 150)}...`
            })
            const hasMore = options.hasMore ?? false
            if (isAppend === false){
                that.setData({
                    discussions: viewData,
                    hasMore: hasMore
                })
            } else {
                that.setData({
                    discussions: that.data.discussions.concat(viewData),
                    loading: false,
                    hasMore: hasMore
                })
            }
            wx.hideNavigationBarLoading()
            wx.stopPullDownRefresh()
        })
    },
    gotoDiscussionsDetail: function(e) {
        gotoDetailPage(e)
    },
    flarumAvatarTap: function(e) {
        gotoProfilePage(e)
    }
});