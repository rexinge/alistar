const json = {
    "links": {
        "first": "https://qizue.com/api/discussions?include=user%2ClastPostedUser%2Ctags%2Ctags.parent%2CfirstPost&sort=&page%5Blimit%5D=20"
    },
    "data": [
        {
            "type": "discussions",
            "id": "4",
            "attributes": {
                "title": "您必须了解的一些注意事项！（随时补充）",
                "slug": "4",
                "commentCount": 1,
                "participantCount": 1,
                "createdAt": "2022-01-22T10:51:49+00:00",
                "lastPostedAt": "2022-01-22T10:51:49+00:00",
                "lastPostNumber": 1,
                "canReply": true,
                "canRename": true,
                "canDelete": true,
                "canHide": true,
                "lastReadAt": "2022-01-22T10:51:49+00:00",
                "lastReadPostNumber": 1,
                "isApproved": true,
                "canSplit": true,
                "canSeeReactions": true,
                "canTag": true,
                "subscription": "follow",
                "isSticky": false,
                "canSticky": true,
                "isLocked": false,
                "canLock": true
            },
            "relationships": {
                "user": {
                    "data": {
                        "type": "users",
                        "id": "1"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "1"
                    }
                },
                "tags": {
                    "data": []
                },
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "6"
                    }
                }
            }
        }
    ],
    "included": [
        {
            "type": "users",
            "id": "1",
            "attributes": {
                "username": "irelia",
                "displayName": "刀锋舞者丶艾瑞莉娅",
                "avatarUrl": "https://qizue.com/assets/avatars/T7rW4QJ0C92t9Mba.png",
                "slug": "irelia"
            }
        },
        {
            "type": "posts",
            "id": "6",
            "attributes": {
                "number": 1,
                "createdAt": "2022-01-22T10:51:49+00:00",
                "contentType": "comment",
                "contentHtml": "<h4>📣社区宣言</h4>\n\n<hr>\n\n<ul><li>🙅‍♂️这里禁止一切违法言论，这既是底线。</li>\n<li>🙅‍♀️请一定不要用标点符号充当标题，发现即处理。</li>\n<li>👮‍♂️我们不接受小广告的骚扰，若您执意，则不受欢迎。</li></ul>\n\n<h4>✏知识点</h4>\n\n<ul><li>新用户注册需要填写可用邮箱，qq，163，gmail等大众邮箱均可，验证后方可正常使用。</li></ul>\n\n<blockquote class=\"uncited\"><div><p>注意：如果找不到验证邮件，很可能在垃圾邮件或者未知邮件分类里！</p></div></blockquote>\n\n<ul><li>新用户注册需要填写英文名称，如需中文，可在个人设置里添加昵称。</li>\n<li>pc端引用快捷方法：鼠标左键点住需要引用的内容拖拽并且点击“引用”即可完成，适用于引用文章内某句某段落。</li>\n<li>鼠标屏幕左边边缘滑动出现主题导航后，如果想固定这种方式阅览，可以点击上方图钉标志按钮，如果想恢复，点掉图钉即可。</li>\n<li>请善加利用@人功能，手动输入@后，敲入特定昵称首字母或中文首字即可选择被@用户。</li>\n<li>大家上传的表情图标或者图片都会保存在个人附件内，可以反复使用，尤其是上传的表情图标，想使用的时候点编辑器左下“我的文件”，打开后就能找到，找到后插入即可。</li>\n<li>手机用户快速已读单帖的姿势：对准主题列表某贴标题向右滑动该标题即可快速已读该帖。</li></ul>\n\n"
            }
        }
    ]
}

module.exports = {json}