import tabbar from "../../tabbar";
import {Tags} from "../../../data/decoder/tags";

Page({
    data: {
        list: tabbar,
        tags: [],
        tagCloud: [],
    },
    onLoad: function (options) {
        const that = this
        that.setData({
            tags: getApp().globalData.flarum.tags
        })
        that.loadData()
    },
    gotoDiscussionByTags: function(e) {
        const description = e.currentTarget.dataset.description,
            icon = e.currentTarget.dataset.icon,
            slug = e.currentTarget.dataset.slug,
            name = e.currentTarget.dataset.name,
            id = e.currentTarget.dataset.id
        const url = `/pages/threads/tags/tags?id=${id}&name=${name}&icon=${icon}&slug=${slug}&description=${description}`
        console.log("gotoDiscussionByTags", url)
        wx.navigateTo({
            url: url
        })
    },
    gotoDiscussionsDetail: function(e) {
        let id = e.currentTarget.dataset.id,
            title = e.currentTarget.dataset.title

        let url = `/pages/threads/detail/detail?sticky=&firstpost={}&users={}&id=${id}&title=${title}&tags={}&slug=`
        wx.navigateTo({
            url: url
        })
    },
    loadData: function() {
        const that = this
        const tags = new Tags()
        tags.execute(function(data, viewData) {
            const tagCloud = []
            viewData.forEach(function(item, key){
                if(item.isChild === true) {
                    delete viewData[key]
                }else if (item.icon === "") {
                    delete viewData[key]
                    tagCloud.push(item)
                }
            });

            const t = []
            viewData.forEach((item, key) => {
                t.push(item)
            })

            that.setData({
                tagCloud: tagCloud,
                tags: t
            })
            wx.hideLoading()
        })
    }
});