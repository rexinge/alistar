export default [{
    pagePath: '/pages/navigator/index/index',
    text: '首页',
    iconPath: '/assets/images/navigator/home.png',
    selectedIconPath: '/assets/images/navigator/home-select.png'
}, {
    pagePath: '/pages/navigator/notification/notification',
    text: '消息',
    iconPath: '/assets/images/navigator/notification.png',
    selectedIconPath: '/assets/images/navigator/notification-select.png'
}, {
    pagePath: '/pages/navigator/tags/tags',
    text: '标签',
    iconPath: '/assets/images/navigator/tags.png',
    selectedIconPath: '/assets/images/navigator/tags-select.png'
}, {
    pagePath: '/pages/navigator/profiles/profiles',
    text: '我的',
    iconPath: '/assets/images/navigator/myself.png',
    selectedIconPath: '/assets/images/navigator/myself-select.png'
}];