import tabbar from "../../tabbar";
import {gotoLogin} from "../../../utils/navigate";
import {Notifications} from "../../../data/decoder/notifications";
import {checkLoginStatus} from "../../../utils/util";

Page({
    data: {
        list: tabbar,
        isLogin: false,
        hasMore: false
    },
    onShow: function() {
        const that = this
        const loginStatus = checkLoginStatus()
        that.setData({
            isLogin: loginStatus.isLogin
        })
    },
    onLoad: function (options) {
        // 看是否登录了
        const that = this
        const loginStatus = checkLoginStatus()
        const isLogin = loginStatus.isLogin
        that.setData({
            isLogin: isLogin
        })

        if (isLogin) {
            that.loadData()
        }
    },
    onPullDownRefresh: function() {
        const that = this
        that.setData({
            page: 1
        })
        wx.showNavigationBarLoading()
        console.log('onPullDownRefresh start')
        that.loadData()
    },
    onReachBottom: function() {
        const that = this
        if (that.data.hasMore === true) {
            return
        }
        wx.showNavigationBarLoading()
        console.log('onReachBottom start')
        that.setData({
            page: that.data.page + 1
        })
        that.loadData(true)
    },
    gotoLogin: function() {
        gotoLogin()
    },
    loadData: function(isAppend = false) {
        const notifications = new Notifications()
        notifications.execute(function(data, viewData, options) {
            console.log(viewData)
        })
    }
});