import flarum from "../../../flarum";
import {gotoLogin} from "../../../utils/navigate";
import {checkLoginStatus} from "../../../utils/util";

Component({
    data: {
        defaultAvatar: flarum.driver.defaultAvatar,
        isLogin: false,
        status: false
    },
    properties: {
        profile: {
            type: Object,
            value: {}
        },
        status: {
            type: Boolean,
            value: false
        }
    },
    externalClasses: ['flarum-avatar-class'],
    attached: function() {
        const that = this
        // 如果需要检测登录状态
        let status = that.properties.status
        let loginStatus = checkLoginStatus()

        that.setData({
            isLogin: loginStatus.isLogin,
            status: status
        })
    },
    methods: {
        flarumAvatarTap: function (e){
            // 采用递归组件方式渲染，不能通过triggerEvent方式向父级传参，可以获取当前页面调用页面方法处理
            const curPages = getCurrentPages();
            const currentPage = curPages[curPages.length - 1];
            if (currentPage && currentPage.flarumAvatarTap) {
                currentPage.flarumAvatarTap(e);
            }
        },
        flarumGotoLoginTap: function(e) {
            gotoLogin()
        }
    }
});
