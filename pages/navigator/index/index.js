import tabbar from '../../tabbar';
import {deleteHtmlTags, urlEncode} from "../../../utils/tools";
import {Discussions} from "../../../data/decoder/discussions";
import {gotoDetailPage, gotoNewDiscussionPage, gotoProfilePage, gotoTagPage} from "../../../utils/navigate";
import flarum from "../../../flarum";

const app = getApp()
Page({
    data: {
        hasMore: false,
        loading: true,
        page: 1,
        list: tabbar,
        statusBarHeight: app.globalData.statusBarHeight,
        discussions: []
    },
    onShow() {
        wx.setNavigationBarTitle({
            title: app.globalData.flarum.title ?? flarum.title
        })
    },
    onLoad: function (options) {
        const that = this
        wx.setNavigationBarTitle({
            title: app.globalData.flarum.title ?? flarum.title
        })
        that.loadData()
    },
    onPullDownRefresh: function() {
        const that = this
        that.setData({
            page: 1,
            hasMore: false
        })
        wx.showNavigationBarLoading()
        that.loadData()
    },
    onReachBottom: function() {
        const that = this
        if (that.data.hasMore === false) {
            return
        }
        wx.showNavigationBarLoading()
        that.setData({
            page: that.data.page + 1
        })
        that.loadData(true)
    },
    clearSearch: function(e) {
        const that = this
        that.loadData(false, "", true)
    },
    search: function(e) {
        const that = this
        let q = e.detail.value
        console.log(`search ${q}`)
        that.loadData(false, q, true)
    },
    loadData: function(isAppend = false, q = "", init = false) {
        const discussions = new Discussions()
        const that = this

        if(init === true) {
            that.setData({
                page: 1,
            })
        }

        const offset = (that.data.page - 1) * 20
        const params = {offset: offset}
        if (q) {
            params.q = q
        }
        discussions.execute(params,function(data, viewData, options) {
            // 如果没有数据返回则，我就认为没有下一页
            if (viewData.length <= 0) {
                that.setData({
                    hasMore: true,
                })
                wx.hideNavigationBarLoading()
                wx.stopPullDownRefresh()
                return
            }

            viewData.forEach(function(item, key){
                viewData[key]["users_json"] = urlEncode(JSON.stringify(item["user"]))
                viewData[key]["tags_json"] = urlEncode(JSON.stringify(item["tags"]))
                viewData[key]["firstpost_json"] = urlEncode(JSON.stringify(item["firstPost"]))

                viewData[key]["firstPost"]["content"] = deleteHtmlTags(item["firstPost"]["contentHtml"])
                viewData[key]["firstPost"]["content"] = `${viewData[key]["firstPost"]["content"].substring(0, 150)}...`
            })
            const hasMore = options.hasMore ?? false
            if (isAppend === false){
                that.setData({
                    discussions: viewData,
                    hasMore: hasMore
                })
            } else {
                that.setData({
                    discussions: that.data.discussions.concat(viewData),
                    loading: false,
                    hasMore: hasMore
                })
            }
            wx.hideNavigationBarLoading()
            wx.stopPullDownRefresh()
        })
    },
    flarumAvatarTap: function(e) {
        gotoProfilePage(e)
    },
    gotoDiscussionByTags: function(e) {
        gotoTagPage(e)
    },
    newDiscussion: function() {
        gotoNewDiscussionPage()
    },
    gotoDiscussionsDetail: function(e) {
        gotoDetailPage(e)
    },
});